package de.starmixcraft.eseynbt.Serializer;

import de.starmixcraft.eseynbt.NBTTagCompound;

public interface NBTSerializer<T> {
	
	public NBTTagCompound toNBT();
	
	
	public T fromNBT(NBTTagCompound compound);

}
