package de.starmixcraft.eseynbt;

public class NBTException extends Exception
{
	private static final long serialVersionUID = 8486465421030134046L;

	public NBTException(String p_i45136_1_)
    {
        super(p_i45136_1_);
    }
}
