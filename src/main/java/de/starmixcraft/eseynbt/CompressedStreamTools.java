package de.starmixcraft.eseynbt;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

import de.starmixcraft.eseynbt.Serializer.NBTSerializer;

public class CompressedStreamTools {
	
	private static int blocksize = 9; //Increase for better compression, 9 is best, 1 is worst!
	
    /**
     * Load the deflatedGZIP compound from the inputstream.
     */
    public static NBTTagCompound readDeflated(InputStream is) throws IOException
    {
        DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new BZip2CompressorInputStream(is)));
        NBTTagCompound nbttagcompound;

        try
        {
            nbttagcompound = read(datainputstream, NBTSizeTracker.INFINITE);
        }
        finally
        {
            datainputstream.close();
        }

        return nbttagcompound;
    }

    /**
     * Write the compound, Deflated and GZIPed, to the outputstream.
     */
    public static void writeDeflated(NBTTagCompound tag, OutputStream outputStream) throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(new BufferedOutputStream(new BZip2CompressorOutputStream(outputStream, blocksize))); 


        try
        {
            write(tag, dataoutputstream);
        }
        finally
        {
            dataoutputstream.close();
        }
    }
    /**
     * Load the gzipped compound from the inputstream.
     */
    public static NBTTagCompound readCompressed(InputStream is) throws IOException
    {
        DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new GZIPInputStream(is)));
        NBTTagCompound nbttagcompound;

        try
        {
            nbttagcompound = read(datainputstream, NBTSizeTracker.INFINITE);
        }
        finally
        {
            datainputstream.close();
        }

        return nbttagcompound;
    }

    /**
     * Write the compound, gzipped, to the outputstream.
     */
    public static void writeCompressed(NBTTagCompound tag, OutputStream outputStream) throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(outputStream)));

        try
        {
            write(tag, dataoutputstream);
        }
        finally
        {
            dataoutputstream.close();
        }
    }

    public static void safeWrite(NBTTagCompound tag, File file) throws IOException
    {
        File file1 = new File(file.getAbsolutePath() + "_tmp");

        if (file1.exists())
        {
            file1.delete();
        }

        write(tag, file1);

        if (file.exists())
        {
            file.delete();
        }

        if (file.exists())
        {
            throw new IOException("Failed to delete " + file);
        }
        else
        {
            file1.renameTo(file);
        }
    }

    public static void write(NBTTagCompound compound, File file) throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(new FileOutputStream(file));

        try
        {
            write(compound, dataoutputstream);
        }
        finally
        {
            dataoutputstream.close();
        }
    }

    public static NBTTagCompound read(File file) throws IOException
    {
        if (!file.exists())
        {
            return null;
        }
        else
        {
            DataInputStream datainputstream = new DataInputStream(new FileInputStream(file));
            NBTTagCompound nbttagcompound;

            try
            {
                nbttagcompound = read(datainputstream, NBTSizeTracker.INFINITE);
            }
            finally
            {
                datainputstream.close();
            }

            return nbttagcompound;
        }
    }

    /**
     * Reads from a CompressedStream.
     */
    public static NBTTagCompound read(DataInputStream inputStream) throws IOException
    {
        return read(inputStream, NBTSizeTracker.INFINITE);
    }

    /**
     * Reads the given DataInput, constructs, and returns an NBTTagCompound with the data from the DataInput
     */
    public static NBTTagCompound read(DataInput data, NBTSizeTracker sizetracker) throws IOException
    {
        NBTBase nbtbase = read(data, 0, sizetracker);

        if (nbtbase instanceof NBTTagCompound)
        {
            return (NBTTagCompound)nbtbase;
        }
        else
        {
            throw new IOException("Root tag must be a named compound tag");
        }
    }

    public static void write(NBTTagCompound nbttagcompound, DataOutput dataoutput) throws IOException
    {
        writeTag(nbttagcompound, dataoutput);
    }

    private static void writeTag(NBTBase nbtbase, DataOutput dataoutput) throws IOException
    {
        dataoutput.writeByte(nbtbase.getId());

        if (nbtbase.getId() != 0)
        {
            dataoutput.writeUTF("");
            nbtbase.write(dataoutput);
        }
    }

    private static NBTBase read(DataInput dataoutput, int length, NBTSizeTracker size) throws IOException
    {
        byte b0 = dataoutput.readByte();

        if (b0 == 0)
        {
            return new NBTTagEnd();
        }
        else
        {
            dataoutput.readUTF();
            NBTBase nbtbase = NBTBase.createNewByType(b0);

            try
            {
                nbtbase.read(dataoutput, length, size);
                return nbtbase;
            }
            catch (IOException ioexception)
            {
            	 System.out.println("Loading NBT data");
                System.out.println("NBT Tag");
                System.out.println("Tag name "+ "[UNNAMED TAG]");
               System.out.println("Tag type "+ Byte.valueOf(b0));
               throw ioexception;
            }
        }
    }
    
    
	public static NBTTagList saveArrayList(ArrayList<NBTSerializer<?>> data) {
		NBTTagList list = new NBTTagList();
		for(int i = 0; i<data.size();i++) {
			list.appendTag(data.get(i).toNBT());
		}
		return list;
	}
    
}
